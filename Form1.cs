﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using DMDatabaseHandler;
using System.IO;

namespace DropMixDatabaseEditor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void cbCardList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearBoxes();
            FillBoxes(Database.databaseEntries[cbCardList.SelectedIndex]);
        }

        private void bSaveCard_Click(object sender, EventArgs e)
        {
            // Clear the boxes if there is supposed to be nothing.
            if (cbInst1.Text == "None")
            {
                cbInst1.Text = "";
            }

            if (cbInst2.Text == "None")
            {
                cbInst2.Text = "";
            }

            if (cbInst3.Text == "None")
            {
                cbInst3.Text = "";
            }

            if (cbInst4.Text == "None")
            {
                cbInst4.Text = "";
            }

            WriteHMX(cbCardList.SelectedIndex);
            MessageBox.Show("Card Changed!");
        }
        private void bAuto_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("TODO");

            int[] keysArray = { 0, 1, 2, 3, 4, 5, 6, -5, -4, -3, -2, -1 };

            switch (cbKey.SelectedIndex)
            {
                case 0:
                    keysArray = new int[] { 0, 1, 2, 3, 4, 5, 6, -5, -4, -3, -2, -1 };
                    break;
                case 1:
                    keysArray = new int[] { -1, 0, 1, 2, 3, 4, 5, 6, -5, -4, -3, -2 };
                    break;
                case 2:
                    keysArray = new int[] { -2, -1, 0, 1, 2, 3, 4, 5, 6, -5, -4, -3 };
                    break;
                case 3:
                    keysArray = new int[] { -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, -5, -4 };
                    break;
                case 4:
                    keysArray = new int[] { -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, -5 };
                    break;
                case 5:
                    keysArray = new int[] { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6 };
                    break;
                case 6:
                    keysArray = new int[] { 6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
                    break;
                case 7:
                    keysArray = new int[] { 5, 6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
                    break;
                case 8:
                    keysArray = new int[] { 4, 5, 6, -5, -4, -3, -2, -1, 0, 1, 2, 3 };
                    break;
                case 9:
                    keysArray = new int[] { 3, 4, 5, 6, -5, -4, -3, -2, -1, 0, 1, 2 };
                    break;
                case 10:
                    keysArray = new int[] { 2, 3, 4, 5, 6, -5, -4, -3, -2, -1, 0, 1 };
                    break;
                case 11:
                    keysArray = new int[] { 1, 2, 3, 4, 5, 6, -5, -4, -3, -2, -1, 0 };
                    break;
                default:
                    keysArray = new int[] { 0, 1, 2, 3, 4, 5, 6, -5, -4, -3, -2, -1 };
                    break;
            }

            tbKeyStepC.Text     = keysArray[0].ToString();
            tbKeyStepDb.Text    = keysArray[1].ToString();
            tbKeyStepD.Text     = keysArray[2].ToString();
            tbKeyStepEb.Text    = keysArray[3].ToString();
            tbKeyStepE.Text     = keysArray[4].ToString();
            tbKeyStepF.Text     = keysArray[5].ToString();
            tbKeyStepGb.Text    = keysArray[6].ToString();
            tbKeyStepG.Text     = keysArray[7].ToString();
            tbKeyStepAb.Text    = keysArray[8].ToString();
            tbKeyStepA.Text     = keysArray[9].ToString();
            tbKeyStepBb.Text    = keysArray[10].ToString();
            tbKeyStepB.Text     = keysArray[11].ToString();


        }

        /// <summary>
        /// Reads a DropMix Database entry, and splits it into a list.
        /// </summary>
        /// <param name="entry">Database entry string.</param>
        public static string[] SplitHMX(string str)
        {

            string[] result = new string[38];
            int index = 0;
            bool inEntry = false;

            Debug.WriteLine($"Attempting to split HMX entry...\n{str}");

            for (int i = 0; i < str.Length; i++)
            {
                if ( str.Substring(i, 1) == "\"" )
                {
                    if (!inEntry)
                    {
                        inEntry = true;
                    }
                    else
                    {
                        inEntry = false;
                    }
                }
                else
                {
                    if (str.Substring(i, 1) == ",")
                    {
                        if (!inEntry)
                        {
                            //Debug.WriteLine(result[index]);
                            index++;
                            inEntry = false;
                        }
                    }
                    else
                    {
                        result[index] += str.Substring(i, 1);
                    }
                }
            }

            return result;

        }

        /// <summary>
        /// Reads a DropMix Database entry, and splits it into a list.
        /// </summary>
        /// <param name="entry">Database entry string.</param>
        public string CreateHMX()
        {
            string[] toWrite = new string[38];
            //toWrite = SplitHMX(Database.databaseEntries[cbCardList.SelectedIndex]);
            string outputString = "";
            int tempInt;

            // this is ugly and I hate this, but whatever
            toWrite[0] = cbCardList.Text.Split(' ')[0];

            toWrite[1] = tbArtist.Text;
            toWrite[2] = tbTitle.Text;
            toWrite[3] = tbAudio.Text;
            toWrite[4] = tbIllustrator.Text;
            toWrite[5] = tbImage.Text;
            toWrite[6] = cbType.Text;
            toWrite[7] = tbBars.Text;
            toWrite[8] = tbPower.Text;

            toWrite[9] = cbInst1.Text;
            toWrite[10] = cbInst2.Text;
            toWrite[11] = cbInst3.Text;
            toWrite[12] = cbInst4.Text;

            toWrite[13] = cbGenre.Text;
            toWrite[14] = tbYear.Text;
            toWrite[15] = tbSource.Text;
            toWrite[16] = tbAbility.Text;
            toWrite[17] = tbScreenText.Text;
            toWrite[18] = tbMusicEffect.Text;
            toWrite[19] = tbBPM.Text;
            toWrite[20] = cbKey.Text;
            toWrite[21] = cbMode.Text;
            toWrite[22] = tbTransition.Text;
            toWrite[23] = tbWildBeatHasKey.Text;
            toWrite[24] = tbArtCropCenter.Text;

            toWrite[25] = tbKeyStepC.Text;
            toWrite[26] = tbKeyStepDb.Text;
            toWrite[27] = tbKeyStepD.Text;
            toWrite[28] = tbKeyStepEb.Text;
            toWrite[29] = tbKeyStepE.Text;
            toWrite[30] = tbKeyStepF.Text;
            toWrite[31] = tbKeyStepGb.Text;
            toWrite[32] = tbKeyStepG.Text;
            toWrite[33] = tbKeyStepAb.Text;
            toWrite[34] = tbKeyStepA.Text;
            toWrite[35] = tbKeyStepBb.Text;
            toWrite[36] = tbKeyStepB.Text;

            toWrite[37] = tbCredits.Text;

            for (int i = 0; i < 38; i++)
            {
                // If it is a number, we don't want it in quotes.
                if (int.TryParse(toWrite[i], out tempInt))
                {
                    outputString += tempInt;
                }
                else
                {
                    // Make sure there is something there before we write anything.
                    if (toWrite[i].Length > 0)
                    {
                        outputString += $"\"{toWrite[i]}\"";
                    }
                }

                // Don't write a comma for the last entry.
                if (i < 37)
                {
                    outputString += ",";
                }

            }

            Debug.WriteLine(outputString);
            return outputString;

        }

        /// <summary>
        /// Writes a DropMix Database entry to the list.
        /// </summary>
        /// <param name="entryNumber">Entry number to write to.</param>
        void WriteHMX(int entryNumber)
        {

            Debug.WriteLine($"Writing entry to database...");

            Database.databaseEntries[entryNumber] = CreateHMX();
            Database.databaseEntriesChanged[entryNumber] = true;

        }

        void FillBoxes(String str)
        {

            var tempEntry = SplitHMX(str);

            tbTitle.Text = tempEntry[2];
            tbArtist.Text = tempEntry[1];

            tbBars.Text = tempEntry[7];
            tbPower.Text = tempEntry[8];
            cbGenre.Text = tempEntry[13];

            cbType.Text = tempEntry[6];
            cbInst1.Text = tempEntry[9];
            cbInst2.Text = tempEntry[10];
            cbInst3.Text = tempEntry[11];
            cbInst4.Text = tempEntry[12];

            tbYear.Text = tempEntry[14];
            cbKey.Text = tempEntry[20];
            cbMode.Text = tempEntry[21];
            tbWildBeatHasKey.Text = tempEntry[23];
            tbBPM.Text = tempEntry[19];

            tbKeyStepC.Text = tempEntry[25];
            tbKeyStepDb.Text = tempEntry[26];
            tbKeyStepD.Text = tempEntry[27];
            tbKeyStepEb.Text = tempEntry[28];
            tbKeyStepE.Text = tempEntry[29];
            tbKeyStepF.Text = tempEntry[30];
            tbKeyStepGb.Text = tempEntry[31];
            tbKeyStepG.Text = tempEntry[32];
            tbKeyStepAb.Text = tempEntry[33];
            tbKeyStepA.Text = tempEntry[34];
            tbKeyStepBb.Text = tempEntry[35];
            tbKeyStepB.Text = tempEntry[36];

            tbAudio.Text = tempEntry[3];
            tbImage.Text = tempEntry[5];
            tbIllustrator.Text = tempEntry[4];
            tbAbility.Text = tempEntry[16];
            tbScreenText.Text = tempEntry[17];
            tbMusicEffect.Text = tempEntry[18];
            tbTransition.Text = tempEntry[22];
            tbArtCropCenter.Text = tempEntry[24];
            tbSource.Text = tempEntry[15];

            tbCredits.Text = tempEntry[37];
        }

        void ClearBoxes()
        {
            tbTitle.Text = "";
            tbArtist.Text = "";

            tbBars.Text = "";
            tbPower.Text = "";
            cbGenre.Text = "";

            cbType.Text = "";
            cbInst1.Text = "";
            cbInst2.Text = "";
            cbInst3.Text = "";
            cbInst4.Text = "";

            tbYear.Text = "";
            cbKey.Text = "";
            cbMode.Text = "";
            tbWildBeatHasKey.Text = "";
            tbBPM.Text = "";

            tbKeyStepC.Text = "";
            tbKeyStepDb.Text = "";
            tbKeyStepD.Text = "";
            tbKeyStepEb.Text = "";
            tbKeyStepE.Text = "";
            tbKeyStepF.Text = "";
            tbKeyStepGb.Text = "";
            tbKeyStepG.Text = "";
            tbKeyStepAb.Text = "";
            tbKeyStepA.Text = "";
            tbKeyStepBb.Text = "";
            tbKeyStepB.Text = "";

            tbAudio.Text = "";
            tbImage.Text = "";
            tbIllustrator.Text = "";
            tbAbility.Text = "";
            tbScreenText.Text = "";
            tbMusicEffect.Text = "";
            tbTransition.Text = "";
            tbArtCropCenter.Text = "";
            tbSource.Text = "";

            tbCredits.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("NOTICE: This is BETA software.\nThis may or may not function properly, keep backups of your databases!");
        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select a DropMix database";
            dialog.Filter = "DAT files|*.dat";
            //dialog.InitialDirectory = @"D:\Sync\DropMix Apk";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Database.OpenDatabase(dialog.FileName.ToString());
                cbCardList.DataSource = Database.databaseEntriesList;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Select a location to save the database";
            dialog.Filter = "DAT files|*.dat";
            //dialog.InitialDirectory = @"D:\Sync\DropMix Apk";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Database.SaveDatabase(dialog.FileName.ToString());
                MessageBox.Show("Database Saved!");
            }
        }

        private void openCardButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select a saved card";
            dialog.Filter = "DM Card files|*.dmcard";
            //dialog.InitialDirectory = @"D:\Sync\DropMix Apk";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                OpenCard(dialog.FileName.ToString());
                cbCardList.DataSource = Database.databaseEntriesList;
            }
        }

        private void saveAsCardButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Select a location to save the card";
            dialog.Filter = "DM Card files|*.dmcard";
            dialog.FileName = tbTitle.Text + " - " + cbType.Text;
            //dialog.InitialDirectory = @"D:\Sync\DropMix Apk";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                SaveCard(dialog.FileName.ToString());
                MessageBox.Show("Card Saved!");
            }
        }


        public void OpenCard(string path)
        {
            Debug.WriteLine("\n\nReading card");
            using (StreamReader reader = File.OpenText(path))
            {
                ClearBoxes();
                FillBoxes(reader.ReadLine());
                reader.Close();
            }
        }

        public void SaveCard(string path)
        {
            Debug.WriteLine("\n\nSaving card");
            using (TextWriter tWriter = File.CreateText(path))
            {
                tWriter.WriteLine(CreateHMX());
                tWriter.Close();
            }
        }

    }
}
