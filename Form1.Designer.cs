﻿namespace DropMixDatabaseEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbCardList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbArtist = new System.Windows.Forms.TextBox();
            this.tbYear = new System.Windows.Forms.TextBox();
            this.tbSource = new System.Windows.Forms.TextBox();
            this.tbKeyStepC = new System.Windows.Forms.TextBox();
            this.tbKeyStepDb = new System.Windows.Forms.TextBox();
            this.tbKeyStepD = new System.Windows.Forms.TextBox();
            this.tbKeyStepF = new System.Windows.Forms.TextBox();
            this.tbKeyStepE = new System.Windows.Forms.TextBox();
            this.tbKeyStepEb = new System.Windows.Forms.TextBox();
            this.tbKeyStepAb = new System.Windows.Forms.TextBox();
            this.tbKeyStepG = new System.Windows.Forms.TextBox();
            this.tbKeyStepGb = new System.Windows.Forms.TextBox();
            this.tbKeyStepB = new System.Windows.Forms.TextBox();
            this.tbKeyStepBb = new System.Windows.Forms.TextBox();
            this.tbKeyStepA = new System.Windows.Forms.TextBox();
            this.tbBars = new System.Windows.Forms.TextBox();
            this.tbPower = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.bAuto = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.tbIllustrator = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbTransition = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbMusicEffect = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbScreenText = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbAbility = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tbImage = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbAudio = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tbArtCropCenter = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cbInst1 = new System.Windows.Forms.ComboBox();
            this.cbInst2 = new System.Windows.Forms.ComboBox();
            this.cbInst3 = new System.Windows.Forms.ComboBox();
            this.cbInst4 = new System.Windows.Forms.ComboBox();
            this.cbGenre = new System.Windows.Forms.ComboBox();
            this.cbKey = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tbCredits = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.bSaveCard = new System.Windows.Forms.Button();
            this.tbBPM = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tbWildBeatHasKey = new System.Windows.Forms.TextBox();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCardButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsCardButton = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseToolStripMenuItem,
            this.cardToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(866, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem1,
            this.saveAsToolStripMenuItem});
            this.databaseToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // cardToolStripMenuItem
            // 
            this.cardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openCardButton,
            this.saveAsCardButton});
            this.cardToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cardToolStripMenuItem.Name = "cardToolStripMenuItem";
            this.cardToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.cardToolStripMenuItem.Text = "Card";
            // 
            // cbCardList
            // 
            this.cbCardList.FormattingEnabled = true;
            this.cbCardList.Location = new System.Drawing.Point(83, 25);
            this.cbCardList.Name = "cbCardList";
            this.cbCardList.Size = new System.Drawing.Size(364, 21);
            this.cbCardList.TabIndex = 1;
            this.cbCardList.SelectedIndexChanged += new System.EventHandler(this.cbCardList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Card";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(83, 52);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(280, 20);
            this.tbTitle.TabIndex = 3;
            // 
            // tbArtist
            // 
            this.tbArtist.Location = new System.Drawing.Point(83, 78);
            this.tbArtist.Name = "tbArtist";
            this.tbArtist.Size = new System.Drawing.Size(280, 20);
            this.tbArtist.TabIndex = 4;
            // 
            // tbYear
            // 
            this.tbYear.Location = new System.Drawing.Point(303, 131);
            this.tbYear.Name = "tbYear";
            this.tbYear.Size = new System.Drawing.Size(60, 20);
            this.tbYear.TabIndex = 9;
            // 
            // tbSource
            // 
            this.tbSource.Location = new System.Drawing.Point(105, 233);
            this.tbSource.Name = "tbSource";
            this.tbSource.Size = new System.Drawing.Size(49, 20);
            this.tbSource.TabIndex = 11;
            // 
            // tbKeyStepC
            // 
            this.tbKeyStepC.Location = new System.Drawing.Point(43, 25);
            this.tbKeyStepC.Name = "tbKeyStepC";
            this.tbKeyStepC.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepC.TabIndex = 12;
            // 
            // tbKeyStepDb
            // 
            this.tbKeyStepDb.Location = new System.Drawing.Point(43, 51);
            this.tbKeyStepDb.Name = "tbKeyStepDb";
            this.tbKeyStepDb.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepDb.TabIndex = 13;
            // 
            // tbKeyStepD
            // 
            this.tbKeyStepD.Location = new System.Drawing.Point(43, 78);
            this.tbKeyStepD.Name = "tbKeyStepD";
            this.tbKeyStepD.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepD.TabIndex = 14;
            // 
            // tbKeyStepF
            // 
            this.tbKeyStepF.Location = new System.Drawing.Point(43, 157);
            this.tbKeyStepF.Name = "tbKeyStepF";
            this.tbKeyStepF.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepF.TabIndex = 17;
            // 
            // tbKeyStepE
            // 
            this.tbKeyStepE.Location = new System.Drawing.Point(43, 130);
            this.tbKeyStepE.Name = "tbKeyStepE";
            this.tbKeyStepE.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepE.TabIndex = 16;
            // 
            // tbKeyStepEb
            // 
            this.tbKeyStepEb.Location = new System.Drawing.Point(43, 104);
            this.tbKeyStepEb.Name = "tbKeyStepEb";
            this.tbKeyStepEb.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepEb.TabIndex = 15;
            // 
            // tbKeyStepAb
            // 
            this.tbKeyStepAb.Location = new System.Drawing.Point(43, 236);
            this.tbKeyStepAb.Name = "tbKeyStepAb";
            this.tbKeyStepAb.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepAb.TabIndex = 20;
            // 
            // tbKeyStepG
            // 
            this.tbKeyStepG.Location = new System.Drawing.Point(43, 209);
            this.tbKeyStepG.Name = "tbKeyStepG";
            this.tbKeyStepG.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepG.TabIndex = 19;
            // 
            // tbKeyStepGb
            // 
            this.tbKeyStepGb.Location = new System.Drawing.Point(43, 183);
            this.tbKeyStepGb.Name = "tbKeyStepGb";
            this.tbKeyStepGb.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepGb.TabIndex = 18;
            // 
            // tbKeyStepB
            // 
            this.tbKeyStepB.Location = new System.Drawing.Point(43, 315);
            this.tbKeyStepB.Name = "tbKeyStepB";
            this.tbKeyStepB.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepB.TabIndex = 23;
            // 
            // tbKeyStepBb
            // 
            this.tbKeyStepBb.Location = new System.Drawing.Point(43, 288);
            this.tbKeyStepBb.Name = "tbKeyStepBb";
            this.tbKeyStepBb.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepBb.TabIndex = 22;
            // 
            // tbKeyStepA
            // 
            this.tbKeyStepA.Location = new System.Drawing.Point(43, 262);
            this.tbKeyStepA.Name = "tbKeyStepA";
            this.tbKeyStepA.Size = new System.Drawing.Size(30, 20);
            this.tbKeyStepA.TabIndex = 21;
            // 
            // tbBars
            // 
            this.tbBars.Location = new System.Drawing.Point(83, 104);
            this.tbBars.Name = "tbBars";
            this.tbBars.Size = new System.Drawing.Size(30, 20);
            this.tbBars.TabIndex = 24;
            // 
            // tbPower
            // 
            this.tbPower.Location = new System.Drawing.Point(83, 130);
            this.tbPower.Name = "tbPower";
            this.tbPower.Size = new System.Drawing.Size(30, 20);
            this.tbPower.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "Title";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Artist";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 186);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Instrument 1";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Instrument 2";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Instrument 3";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 267);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Instrument 4";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "C";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Db";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "D";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 35;
            this.label11.Text = "Eb";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "E";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 160);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(13, 13);
            this.label13.TabIndex = 37;
            this.label13.Text = "F";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 186);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(21, 13);
            this.label14.TabIndex = 38;
            this.label14.Text = "Gb";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 212);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "G";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 239);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 40;
            this.label16.Text = "Ab";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 265);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "A";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 291);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Bb";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 318);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 13);
            this.label19.TabIndex = 43;
            this.label19.Text = "B";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.bAuto);
            this.panel1.Controls.Add(this.tbKeyStepC);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.tbKeyStepDb);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.tbKeyStepD);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.tbKeyStepEb);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.tbKeyStepE);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.tbKeyStepF);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.tbKeyStepGb);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.tbKeyStepG);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.tbKeyStepAb);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.tbKeyStepA);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tbKeyStepBb);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.tbKeyStepB);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(480, 36);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(90, 397);
            this.panel1.TabIndex = 44;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 45;
            this.label32.Text = "Key Steps";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bAuto
            // 
            this.bAuto.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bAuto.Location = new System.Drawing.Point(19, 341);
            this.bAuto.Name = "bAuto";
            this.bAuto.Size = new System.Drawing.Size(54, 23);
            this.bAuto.TabIndex = 44;
            this.bAuto.Text = "Auto";
            this.bAuto.UseVisualStyleBackColor = true;
            this.bAuto.Click += new System.EventHandler(this.bAuto_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(49, 107);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Bars";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(40, 133);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(37, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "Power";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label41);
            this.panel2.Controls.Add(this.tbIllustrator);
            this.panel2.Controls.Add(this.label40);
            this.panel2.Controls.Add(this.tbTransition);
            this.panel2.Controls.Add(this.label39);
            this.panel2.Controls.Add(this.tbMusicEffect);
            this.panel2.Controls.Add(this.label38);
            this.panel2.Controls.Add(this.tbScreenText);
            this.panel2.Controls.Add(this.label37);
            this.panel2.Controls.Add(this.tbAbility);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.tbImage);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.tbAudio);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.tbArtCropCenter);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.tbSource);
            this.panel2.Location = new System.Drawing.Point(576, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(279, 397);
            this.panel2.TabIndex = 47;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(50, 80);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(49, 13);
            this.label41.TabIndex = 63;
            this.label41.Text = "Illustrator";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbIllustrator
            // 
            this.tbIllustrator.Location = new System.Drawing.Point(105, 77);
            this.tbIllustrator.Name = "tbIllustrator";
            this.tbIllustrator.Size = new System.Drawing.Size(160, 20);
            this.tbIllustrator.TabIndex = 62;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(46, 184);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 13);
            this.label40.TabIndex = 61;
            this.label40.Text = "Transition";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbTransition
            // 
            this.tbTransition.Location = new System.Drawing.Point(105, 181);
            this.tbTransition.Name = "tbTransition";
            this.tbTransition.Size = new System.Drawing.Size(160, 20);
            this.tbTransition.TabIndex = 60;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(33, 158);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(66, 13);
            this.label39.TabIndex = 59;
            this.label39.Text = "Music Effect";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbMusicEffect
            // 
            this.tbMusicEffect.Location = new System.Drawing.Point(105, 155);
            this.tbMusicEffect.Name = "tbMusicEffect";
            this.tbMusicEffect.Size = new System.Drawing.Size(160, 20);
            this.tbMusicEffect.TabIndex = 58;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(34, 132);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 13);
            this.label38.TabIndex = 57;
            this.label38.Text = "Screen Text";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbScreenText
            // 
            this.tbScreenText.Location = new System.Drawing.Point(105, 129);
            this.tbScreenText.Name = "tbScreenText";
            this.tbScreenText.Size = new System.Drawing.Size(160, 20);
            this.tbScreenText.TabIndex = 56;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(65, 106);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(34, 13);
            this.label37.TabIndex = 55;
            this.label37.Text = "Ability";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbAbility
            // 
            this.tbAbility.Location = new System.Drawing.Point(105, 103);
            this.tbAbility.Name = "tbAbility";
            this.tbAbility.Size = new System.Drawing.Size(160, 20);
            this.tbAbility.TabIndex = 54;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(63, 54);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 13);
            this.label28.TabIndex = 53;
            this.label28.Text = "Image";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbImage
            // 
            this.tbImage.Location = new System.Drawing.Point(105, 51);
            this.tbImage.Name = "tbImage";
            this.tbImage.Size = new System.Drawing.Size(160, 20);
            this.tbImage.TabIndex = 52;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(65, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(34, 13);
            this.label27.TabIndex = 51;
            this.label27.Text = "Audio";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbAudio
            // 
            this.tbAudio.Location = new System.Drawing.Point(105, 25);
            this.tbAudio.Name = "tbAudio";
            this.tbAudio.Size = new System.Drawing.Size(160, 20);
            this.tbAudio.TabIndex = 50;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(58, 236);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 49;
            this.label25.Text = "Source";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(20, 210);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 13);
            this.label23.TabIndex = 48;
            this.label23.Text = "Art Crop Center";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tbArtCropCenter
            // 
            this.tbArtCropCenter.Location = new System.Drawing.Point(105, 207);
            this.tbArtCropCenter.Name = "tbArtCropCenter";
            this.tbArtCropCenter.Size = new System.Drawing.Size(49, 20);
            this.tbArtCropCenter.TabIndex = 47;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "Advanced";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(141, 107);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(36, 13);
            this.label24.TabIndex = 48;
            this.label24.Text = "Genre";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(268, 134);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 13);
            this.label26.TabIndex = 49;
            this.label26.Text = "Year";
            // 
            // cbInst1
            // 
            this.cbInst1.DropDownWidth = 80;
            this.cbInst1.FormattingEnabled = true;
            this.cbInst1.Items.AddRange(new object[] {
            "Vocal",
            "Horns",
            "Synth",
            "Guitar",
            "Drums",
            "Sampler",
            "None"});
            this.cbInst1.Location = new System.Drawing.Point(83, 183);
            this.cbInst1.Name = "cbInst1";
            this.cbInst1.Size = new System.Drawing.Size(80, 21);
            this.cbInst1.TabIndex = 5;
            // 
            // cbInst2
            // 
            this.cbInst2.DropDownWidth = 80;
            this.cbInst2.FormattingEnabled = true;
            this.cbInst2.Items.AddRange(new object[] {
            "Vocal",
            "Horns",
            "Synth",
            "Guitar",
            "Drums",
            "Sampler",
            "None"});
            this.cbInst2.Location = new System.Drawing.Point(83, 210);
            this.cbInst2.Name = "cbInst2";
            this.cbInst2.Size = new System.Drawing.Size(80, 21);
            this.cbInst2.TabIndex = 50;
            // 
            // cbInst3
            // 
            this.cbInst3.DropDownWidth = 80;
            this.cbInst3.FormattingEnabled = true;
            this.cbInst3.Items.AddRange(new object[] {
            "Vocal",
            "Horns",
            "Synth",
            "Guitar",
            "Drums",
            "Sampler",
            "None"});
            this.cbInst3.Location = new System.Drawing.Point(83, 237);
            this.cbInst3.Name = "cbInst3";
            this.cbInst3.Size = new System.Drawing.Size(80, 21);
            this.cbInst3.TabIndex = 51;
            // 
            // cbInst4
            // 
            this.cbInst4.DropDownWidth = 80;
            this.cbInst4.FormattingEnabled = true;
            this.cbInst4.Items.AddRange(new object[] {
            "Vocal",
            "Horns",
            "Synth",
            "Guitar",
            "Drums",
            "Sampler",
            "None"});
            this.cbInst4.Location = new System.Drawing.Point(83, 264);
            this.cbInst4.Name = "cbInst4";
            this.cbInst4.Size = new System.Drawing.Size(80, 21);
            this.cbInst4.TabIndex = 52;
            // 
            // cbGenre
            // 
            this.cbGenre.FormattingEnabled = true;
            this.cbGenre.Items.AddRange(new object[] {
            "genre_fx",
            "genre_danceelec",
            "genre_pop",
            "genre_country",
            "genre_soundtrack",
            "genre_rock",
            "genre_hiphop",
            "genre_latin",
            "genre_rnb",
            "genre_classical"});
            this.cbGenre.Location = new System.Drawing.Point(183, 104);
            this.cbGenre.Name = "cbGenre";
            this.cbGenre.Size = new System.Drawing.Size(180, 21);
            this.cbGenre.TabIndex = 53;
            // 
            // cbKey
            // 
            this.cbKey.FormattingEnabled = true;
            this.cbKey.Items.AddRange(new object[] {
            "C",
            "Db",
            "D",
            "Eb",
            "E",
            "F",
            "Gb",
            "G",
            "Ab",
            "A",
            "Bb",
            "B"});
            this.cbKey.Location = new System.Drawing.Point(303, 157);
            this.cbKey.Name = "cbKey";
            this.cbKey.Size = new System.Drawing.Size(60, 21);
            this.cbKey.TabIndex = 54;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(272, 160);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(25, 13);
            this.label29.TabIndex = 55;
            this.label29.Text = "Key";
            // 
            // cbMode
            // 
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Items.AddRange(new object[] {
            "major",
            "minor"});
            this.cbMode.Location = new System.Drawing.Point(303, 184);
            this.cbMode.Name = "cbMode";
            this.cbMode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cbMode.Size = new System.Drawing.Size(60, 21);
            this.cbMode.TabIndex = 56;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(263, 187);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 57;
            this.label30.Text = "Mode";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(38, 294);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(39, 13);
            this.label31.TabIndex = 59;
            this.label31.Text = "Credits";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCredits
            // 
            this.tbCredits.Location = new System.Drawing.Point(83, 291);
            this.tbCredits.Multiline = true;
            this.tbCredits.Name = "tbCredits";
            this.tbCredits.Size = new System.Drawing.Size(280, 85);
            this.tbCredits.TabIndex = 58;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.bSaveCard);
            this.panel3.Controls.Add(this.tbBPM);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Controls.Add(this.cbType);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.label34);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.tbWildBeatHasKey);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.cbCardList);
            this.panel3.Controls.Add(this.tbCredits);
            this.panel3.Controls.Add(this.tbTitle);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.tbArtist);
            this.panel3.Controls.Add(this.cbMode);
            this.panel3.Controls.Add(this.cbInst1);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.tbYear);
            this.panel3.Controls.Add(this.cbKey);
            this.panel3.Controls.Add(this.tbBars);
            this.panel3.Controls.Add(this.cbGenre);
            this.panel3.Controls.Add(this.tbPower);
            this.panel3.Controls.Add(this.cbInst4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.cbInst3);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.cbInst2);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Location = new System.Drawing.Point(12, 36);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(462, 397);
            this.panel3.TabIndex = 60;
            // 
            // bSaveCard
            // 
            this.bSaveCard.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bSaveCard.Location = new System.Drawing.Point(372, 353);
            this.bSaveCard.Name = "bSaveCard";
            this.bSaveCard.Size = new System.Drawing.Size(75, 23);
            this.bSaveCard.TabIndex = 61;
            this.bSaveCard.Text = "Write Card";
            this.bSaveCard.UseVisualStyleBackColor = true;
            this.bSaveCard.Click += new System.EventHandler(this.bSaveCard_Click);
            // 
            // tbBPM
            // 
            this.tbBPM.Location = new System.Drawing.Point(303, 237);
            this.tbBPM.Name = "tbBPM";
            this.tbBPM.Size = new System.Drawing.Size(60, 20);
            this.tbBPM.TabIndex = 63;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(268, 240);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(30, 13);
            this.label36.TabIndex = 64;
            this.label36.Text = "BPM";
            // 
            // cbType
            // 
            this.cbType.DropDownWidth = 80;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Lead",
            "Loop",
            "Beat",
            "Bass",
            "Wild"});
            this.cbType.Location = new System.Drawing.Point(83, 156);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(80, 21);
            this.cbType.TabIndex = 61;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(46, 159);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(31, 13);
            this.label35.TabIndex = 62;
            this.label35.Text = "Type";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(201, 214);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(96, 13);
            this.label34.TabIndex = 55;
            this.label34.Text = "Wild Beat Has Key";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(3, 4);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(59, 13);
            this.label33.TabIndex = 60;
            this.label33.Text = "Information";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbWildBeatHasKey
            // 
            this.tbWildBeatHasKey.Location = new System.Drawing.Point(303, 211);
            this.tbWildBeatHasKey.Name = "tbWildBeatHasKey";
            this.tbWildBeatHasKey.Size = new System.Drawing.Size(60, 20);
            this.tbWildBeatHasKey.TabIndex = 54;
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // openCardButton
            // 
            this.openCardButton.Name = "openCardButton";
            this.openCardButton.Size = new System.Drawing.Size(180, 22);
            this.openCardButton.Text = "Open";
            this.openCardButton.Click += new System.EventHandler(this.openCardButton_Click);
            // 
            // saveAsCardButton
            // 
            this.saveAsCardButton.Name = "saveAsCardButton";
            this.saveAsCardButton.Size = new System.Drawing.Size(180, 22);
            this.saveAsCardButton.Text = "Save As...";
            this.saveAsCardButton.Click += new System.EventHandler(this.saveAsCardButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(866, 446);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DropMix Database Editor";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox tbArtist;
        private System.Windows.Forms.TextBox tbYear;
        private System.Windows.Forms.TextBox tbSource;
        private System.Windows.Forms.TextBox tbKeyStepC;
        private System.Windows.Forms.TextBox tbKeyStepDb;
        private System.Windows.Forms.TextBox tbKeyStepD;
        private System.Windows.Forms.TextBox tbKeyStepF;
        private System.Windows.Forms.TextBox tbKeyStepE;
        private System.Windows.Forms.TextBox tbKeyStepEb;
        private System.Windows.Forms.TextBox tbKeyStepAb;
        private System.Windows.Forms.TextBox tbKeyStepG;
        private System.Windows.Forms.TextBox tbKeyStepGb;
        private System.Windows.Forms.TextBox tbKeyStepB;
        private System.Windows.Forms.TextBox tbKeyStepBb;
        private System.Windows.Forms.TextBox tbKeyStepA;
        private System.Windows.Forms.TextBox tbBars;
        private System.Windows.Forms.TextBox tbPower;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bAuto;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbArtCropCenter;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cbInst1;
        private System.Windows.Forms.ComboBox cbInst2;
        private System.Windows.Forms.ComboBox cbInst3;
        private System.Windows.Forms.ComboBox cbInst4;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tbImage;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbAudio;
        private System.Windows.Forms.ComboBox cbGenre;
        private System.Windows.Forms.ComboBox cbKey;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbCredits;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbBPM;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbWildBeatHasKey;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbTransition;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbMusicEffect;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbScreenText;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbAbility;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbIllustrator;
        public System.Windows.Forms.ComboBox cbCardList;
        private System.Windows.Forms.Button bSaveCard;
        private System.Windows.Forms.ToolStripMenuItem cardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openCardButton;
        private System.Windows.Forms.ToolStripMenuItem saveAsCardButton;
    }
}

