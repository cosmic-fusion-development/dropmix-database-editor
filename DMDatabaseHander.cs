﻿using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Collections.Generic;
using DropMixDatabaseEditor;
using System.Text;

namespace DMDatabaseHandler {

	public class Database
    {

        // TODO: Mystery stuff. Determine if any of this is useful later.

        // Header as of DropMix 1.9.0.
        // There are 441 entries in the database, so the last int is the number of entries.
        static int[] databaseHeader =
        {
            0,2450,0,1,
            1,169,0,0,
            3,2,711,0,
            2,687,0,2,
            705,0,3,2,
            692,0,2,702,
            0,2,706,0,
            0,441
        };

        // Make lists for storing things.
        public static List<string> databaseEntries = new List<string>();
        public static List<byte[]> databaseEntriesRaw = new List<byte[]>();
        public static List<string> databaseEntriesList = new List<string>();
        public static List<bool> databaseEntriesChanged = new List<bool>();

        /// <summary>
        /// Reads a DropMix Database, and gets the information.
        /// </summary>
        /// <param name="file">File path to a DropMix Database.</param>
        public static void OpenDatabase(string path) 
        {

            Debug.WriteLine("Opening database...");

            // Clear out these variables on a new load.
            databaseEntries = new List<string>();
            databaseEntriesRaw = new List<byte[]>();
            databaseEntriesList = new List<string>();
            databaseEntriesChanged = new List<bool>();

            // Count how many times we fail to recreate an entry.
            int failedCount = 0;

            using (BinaryReader bReader = new BinaryReader(File.Open(path, FileMode.Open)))
            {
                bReader.BaseStream.Position = 0;

                // Header stuff.
                // ======================================================================

                Debug.WriteLine("Header of Database...");

                // Check the beginning of the database to determine if we are actually working with the database.
                // Note: Since the header includes the entry amount, this can cause incompatibilities with expanded databases.
                // As it is currently not possible to create our own cards, this is not an issue at the moment.

                bool ignoreReadErrors = false;
                for (int i = 0; i < 30; i++)
                {
                    int tempRead;
                    tempRead = bReader.ReadInt32();

                    if ( tempRead != databaseHeader[i] && !ignoreReadErrors)
                    {
                        Debug.WriteLine($"Error: Header of database is incorrect, potentially corrupt database?");
                        var result = MessageBox.Show($"Header of database does not match DropMix 1.9.0.\nAttempt to load anyway?", "Error",MessageBoxButtons.YesNo);

                        if (result == System.Windows.Forms.DialogResult.Yes) 
                        {
                            ignoreReadErrors = true;
                        }
                        else
                        {
                            return;
                        }
                        
                    }

                    //Debug.Write(tempRead.ToString() + ",");

                }


                // We read the file four bytes at a time, in a similar way that DropMix does, so make sure we can read 4 bytes.
                while (bReader.BaseStream.Position <= bReader.BaseStream.Length-4)
                {

                    // Read Length
                    // ======================================================================

                    // We clear the array on every loop.
                    byte[] readLengthBytes = { 0, 0, 0, 0 };

                    // Fill the array with the next four bytes.
                    for (int i = 0; i < 4; i++)
                    {
                        readLengthBytes[i] += bReader.ReadByte();
                    }

                    // Convert the four bytes to an int, to determine the length that we should read.
                    int readLength = BitConverter.ToInt32(readLengthBytes, 0);

                    // Determine how many sets of bytes we need to read in order to get all of the info.
                    int readsToDo = (int)Math.Ceiling(readLength / 4f);

                    Debug.WriteLine($"Reading {readsToDo} times...");

                    //Debug.WriteLine($"Length Bytes: {readLengthBytes[0]} {readLengthBytes[1]} {readLengthBytes[2]} {readLengthBytes[3]}");
                    //Debug.WriteLine($"Read Length: {readLength}");

                    // Check the read length for anything that is obnoxiously large, as this means 
                    // we are not reading the right info, and the database is not sane.
                    if (readLength > 9000)
                    {
                        Debug.WriteLine($"Error: Read Length is very large. Potentially corrupt database.");
                        MessageBox.Show($"Error: Read Length is very large. ({readLength}) Potentially corrupt database.");
                        return;
                    }


                    // Read Entry
                    // ======================================================================

                    int bytePosition = 0;
                    byte[] entryBytes = new byte[readLength];
                    string entryString = "";

                    // Do the reading.
                    for (int i = 0; i < readsToDo; i++)
                    {
                        // Create a variable for storing the bytes for this read.
                        byte[] tempRead = new byte[4];
                        bool stringEnded = false;

                        // Read the four bytes.
                        for (int j = 0; j < 4; j++)
                        {
                            tempRead[j] += bReader.ReadByte();

                            // "13" is the byte that is used at the end of the string, and is included in this read.
                            // We don't need anymore when reading, so we stop adding info here.
                            if (tempRead[j] == 13)
                            {
                                stringEnded = true;
                                readLength = bytePosition;
                                Array.Resize(ref entryBytes, readLength);
                                Debug.WriteLine($"Found end of string. {3 - j} bytes left to read.");
                            }

                            if ( !stringEnded )
                            {
                                if ( bytePosition < readLength )
                                {
                                    entryBytes[bytePosition] = tempRead[j];
                                    bytePosition++;
                                }
                            }
                            
                        }

                    }

                    // Convert Entry to String
                    // ======================================================================

                    // Add the characters to our string.
                    for (int k = 0; k < readLength; k++)
                    {

                        // If we run into a fun character, convert that.
                        if (entryBytes[k] >= 194)
                        {
                            byte[] tempEncode = { entryBytes[k] , entryBytes[k + 1] };

                            string result = "";

                            result = Encoding.UTF8.GetString(tempEncode);

                            Debug.WriteLine(result);
                            entryString += result;
                            k++;
                        }
                        else
                        {
                            entryString += (char)entryBytes[k];
                        }

                    }

                    Debug.WriteLine(entryString);


                    // Convert our string back into bytes, and test to see if it is correct.
                    byte[] byteCompareArray = Encoding.UTF8.GetBytes(entryString);

                    string byteStringTest1 = "";
                    string byteStringTest2 = "";

                    for (int i = 0; i < entryBytes.Length; i++)
                    {
                        byteStringTest1 += $"{entryBytes[i].ToString().PadRight(4)}";
                    }

                    for (int i = 0; i < byteCompareArray.Length; i++)
                    {
                        byteStringTest2 += $"{byteCompareArray[i].ToString().PadRight(4)}";
                    }

                    //Debug.WriteLine(byteStringTest1);
                    //Debug.WriteLine(byteStringTest2);

                    // Check for any differences.
                    if (byteStringTest1 != byteStringTest2)
                    {
                        Debug.WriteLine("Failed to recreate bytes from database.");
                        failedCount++;
                    }

                    Debug.WriteLine("");

                    // Add the entry to the database.
                    databaseEntries.Add(entryString);
                    databaseEntriesRaw.Add(entryBytes);
                    databaseEntriesChanged.Add(false);

                } // End Read Loop

            }

            Debug.WriteLine($"Processed {databaseEntries.Count} entries.");
            Debug.WriteLine($"Failed to recreate {failedCount} entries.");

            if (failedCount > 0)
            {
                MessageBox.Show($"Importer failed to recreate {failedCount} entries in database.\nThey will be wrote back as is, as long as they are not changed.");
            }

            for (int i = 0; i < databaseEntries.Count; i++)
            {
                var EntryString = "";

                var tempEntry = Form1.SplitHMX(databaseEntries[i]);

                EntryString += tempEntry[0].Replace("\"", "");
                EntryString += " - ";
                EntryString += tempEntry[2].Replace("\"", "");

                databaseEntriesList.Add(EntryString);

            }

        }

        /// <summary>
        /// Saves a DropMix Database to a specified location.
        /// </summary>
        /// <param name="file">File path to a DropMix Database.</param>
        public static void SaveDatabase(string path)
        {
            Debug.WriteLine("\n\nSaving database");

            using (BinaryWriter bWriter = new BinaryWriter(File.Open(path, FileMode.Create)))
            {
                bWriter.BaseStream.Position = 0;

                Debug.WriteLine("Writing header of database...");

                // Write the header to the database.
                for (int i = 0; i < 30; i++)
                {
                    bWriter.Write(databaseHeader[i]);
                }

                // Write all of the entries to the database.
                for (int i = 0; i < databaseEntries.Count; i++)
                {
                    var tempString  = databaseEntries[i];
                    byte[] tempBytes = databaseEntriesRaw[i];


                    // Check if we actually changed anything, otherwise just write the raw bytes back.
                    if (databaseEntriesChanged[i])
                    {
                        var tempNewBytes = Encoding.UTF8.GetBytes(tempString);
                        tempBytes = new byte[tempNewBytes.Length];
                        tempBytes = tempNewBytes;
                    }
                    else
                    {
                        tempBytes = databaseEntriesRaw[i];
                    }

                    
                    List<byte> writeBytes = new List<byte>();

                    for (int j = 0; j < tempBytes.Length; j++)
                    {
                        writeBytes.Add(tempBytes[j]);
                    }

                    // The last entry does not end with byte 13, so we add it in every other case.
                    if (i != databaseEntries.Count - 1)
                    {
                        writeBytes.Add((byte)13);
                    }

                    Debug.WriteLine($"Writing length {writeBytes.Count} of entry...");
                    bWriter.Write(writeBytes.Count);

                    // If the result is not divisible by 4, it would not be a vaild entry, so we pad it.
                    while ( Math.Ceiling(writeBytes.Count / 4d) != (writeBytes.Count / 4d))
                    {
                        writeBytes.Add((byte)0);
                    }

                    Debug.WriteLine($"Writing entry: {tempString}");
                    for (int k = 0; k < writeBytes.Count; k++)
                    {
                        bWriter.Write((byte)writeBytes[k]);
                    }

                    
                }


            }

        }

	}

}
